﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace submitter.DAL
{
    public enum StocksList
    {
        ShutterStock,
        AdobeStock,
        DreamsTime,
        Depositphotos,
        RoyaltyFree,
        Bigstock,
        VectorStock
    }
}
