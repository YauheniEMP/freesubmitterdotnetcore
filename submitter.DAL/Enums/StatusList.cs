﻿using System;
using System.Collections.Generic;
using System.Text;

namespace submitter.DAL.Enums
{
    public enum StatusList
    {
        Uploaded,
        InProcess,
        Submitted,
        Failed,
        NotSelected
    }
}
