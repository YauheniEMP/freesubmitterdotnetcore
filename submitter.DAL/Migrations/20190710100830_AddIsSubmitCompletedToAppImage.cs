﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace submitter.DAL.Migrations
{
    public partial class AddIsSubmitCompletedToAppImage : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "IsSubmitCompleted",
                table: "AppImages",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsSubmitCompleted",
                table: "AppImages");
        }
    }
}
