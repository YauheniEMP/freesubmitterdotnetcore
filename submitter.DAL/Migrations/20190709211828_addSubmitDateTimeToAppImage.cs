﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace submitter.DAL.Migrations
{
    public partial class addSubmitDateTimeToAppImage : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "SubmitDateTime",
                table: "AppImages",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "SubmitDateTime",
                table: "AppImages");
        }
    }
}
