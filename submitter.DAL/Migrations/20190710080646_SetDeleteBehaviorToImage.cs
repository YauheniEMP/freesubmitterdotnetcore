﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace submitter.DAL.Migrations
{
    public partial class SetDeleteBehaviorToImage : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ImageStatuses_AppImages_AppImageId",
                table: "ImageStatuses");

            migrationBuilder.AddForeignKey(
                name: "FK_ImageStatuses_AppImages_AppImageId",
                table: "ImageStatuses",
                column: "AppImageId",
                principalTable: "AppImages",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ImageStatuses_AppImages_AppImageId",
                table: "ImageStatuses");

            migrationBuilder.AddForeignKey(
                name: "FK_ImageStatuses_AppImages_AppImageId",
                table: "ImageStatuses",
                column: "AppImageId",
                principalTable: "AppImages",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
