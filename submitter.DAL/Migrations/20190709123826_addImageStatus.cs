﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace submitter.DAL.Migrations
{
    public partial class addImageStatus : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsSubmited",
                table: "AppImages");

            migrationBuilder.DropColumn(
                name: "SubmitDate",
                table: "AppImages");

            migrationBuilder.DropColumn(
                name: "SubmitResult",
                table: "AppImages");

            migrationBuilder.DropColumn(
                name: "UploadedToAdobestock",
                table: "AppImages");

            migrationBuilder.DropColumn(
                name: "UploadedToBigstock",
                table: "AppImages");

            migrationBuilder.DropColumn(
                name: "UploadedToDepositphotos",
                table: "AppImages");

            migrationBuilder.DropColumn(
                name: "UploadedToDreamstime",
                table: "AppImages");

            migrationBuilder.DropColumn(
                name: "UploadedToGettyImages",
                table: "AppImages");

            migrationBuilder.DropColumn(
                name: "UploadedToRoyaltyFree",
                table: "AppImages");

            migrationBuilder.DropColumn(
                name: "UploadedToShutterstock",
                table: "AppImages");

            migrationBuilder.RenameColumn(
                name: "UploadedToVectorstock",
                table: "AppImages",
                newName: "IsSubmitted");

            migrationBuilder.CreateTable(
                name: "ImageStatus",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    Stock = table.Column<int>(nullable: false),
                    Status = table.Column<int>(nullable: false),
                    SubmitDateTime = table.Column<DateTime>(nullable: false),
                    AppImageId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ImageStatus", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ImageStatus_AppImages_AppImageId",
                        column: x => x.AppImageId,
                        principalTable: "AppImages",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ImageStatus_AppImageId",
                table: "ImageStatus",
                column: "AppImageId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ImageStatus");

            migrationBuilder.RenameColumn(
                name: "IsSubmitted",
                table: "AppImages",
                newName: "UploadedToVectorstock");

            migrationBuilder.AddColumn<bool>(
                name: "IsSubmited",
                table: "AppImages",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<DateTime>(
                name: "SubmitDate",
                table: "AppImages",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<bool>(
                name: "SubmitResult",
                table: "AppImages",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "UploadedToAdobestock",
                table: "AppImages",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "UploadedToBigstock",
                table: "AppImages",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "UploadedToDepositphotos",
                table: "AppImages",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "UploadedToDreamstime",
                table: "AppImages",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "UploadedToGettyImages",
                table: "AppImages",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "UploadedToRoyaltyFree",
                table: "AppImages",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "UploadedToShutterstock",
                table: "AppImages",
                nullable: false,
                defaultValue: false);
        }
    }
}
