﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace submitter.DAL.Migrations
{
    public partial class addImageStatusFK1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ImageStatus_AppImages_AppImageId",
                table: "ImageStatus");

            migrationBuilder.DropPrimaryKey(
                name: "PK_ImageStatus",
                table: "ImageStatus");

            migrationBuilder.RenameTable(
                name: "ImageStatus",
                newName: "ImageStatuses");

            migrationBuilder.RenameIndex(
                name: "IX_ImageStatus_AppImageId",
                table: "ImageStatuses",
                newName: "IX_ImageStatuses_AppImageId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_ImageStatuses",
                table: "ImageStatuses",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_ImageStatuses_AppImages_AppImageId",
                table: "ImageStatuses",
                column: "AppImageId",
                principalTable: "AppImages",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ImageStatuses_AppImages_AppImageId",
                table: "ImageStatuses");

            migrationBuilder.DropPrimaryKey(
                name: "PK_ImageStatuses",
                table: "ImageStatuses");

            migrationBuilder.RenameTable(
                name: "ImageStatuses",
                newName: "ImageStatus");

            migrationBuilder.RenameIndex(
                name: "IX_ImageStatuses_AppImageId",
                table: "ImageStatus",
                newName: "IX_ImageStatus_AppImageId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_ImageStatus",
                table: "ImageStatus",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_ImageStatus_AppImages_AppImageId",
                table: "ImageStatus",
                column: "AppImageId",
                principalTable: "AppImages",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
