﻿using System;
using System.Collections.Generic;
using System.Text;

namespace submitter.DAL.Models
{
    public class AppImage
    {
        public string Id { get; set; }
        public string UserId { get; set; }
        public User User { get; set; }
        public string Name { get; set; }
        public DateTime DateUpload { get; set; }
        public string Title { get; set; }
        public string Keywords { get; set; }
        public List<ImageStatus> ImageStatus { get; set; }
        public bool IsSubmitted { get; set; }
        public DateTime SubmitDateTime { get; set; }
        public bool IsSubmitCompleted { get; set; }
    }
}
