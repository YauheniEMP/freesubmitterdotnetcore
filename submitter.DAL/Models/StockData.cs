﻿using System;
using System.Collections.Generic;
using System.Text;

namespace submitter.DAL.Models
{
    public class StockData
    {
        public string Id { get; set; }
        public string UserId { get; set; }
        public User User { get; set; }
        public StocksList Stock { get; set; }
        public bool Permission { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
    }
}
