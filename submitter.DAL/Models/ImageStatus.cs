﻿using submitter.DAL.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace submitter.DAL.Models
{
    public class ImageStatus
    {
        public string Id { get; set; }
        public StocksList Stock { get; set; }
        public StatusList Status { get; set; }
        public DateTime SubmitDateTime { get; set; }
        public string AppImageId { get; set; }
        public AppImage AppImage { get; set; }
    }
}
