﻿using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;

namespace submitter.DAL.Models
{
    public class User : IdentityUser
    {
       public List<StockData> StockData { get; set; }
        public List<AppImage> AppImages { get; set; }
    }
}
