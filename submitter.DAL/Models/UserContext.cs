﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace submitter.DAL.Models
{
    public class UserContext : IdentityDbContext<User>
    {
        public DbSet<AppImage> AppImages { get; set; }
        public DbSet<StockData> StocksData { get; set; }
        public DbSet<ImageStatus> ImageStatuses { get; set; }
        public UserContext(DbContextOptions options) : base(options)
        {

        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity("submitter.DAL.Models.ImageStatus", b =>
            {
                b.HasOne("submitter.DAL.Models.AppImage", "AppImage")
                    .WithMany("ImageStatus")
                    .HasForeignKey("AppImageId")
                    .OnDelete(DeleteBehavior.Cascade);
            });
        }
    }
}
