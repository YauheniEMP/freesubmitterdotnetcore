﻿using Microsoft.EntityFrameworkCore;
using submitter.DAL.Models;
using submitter.DAL.Repositories.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace submitter.DAL.Repositories
{
    public class AppImageRepository : IAppImageRepository
    {
        private readonly UserContext _context;
        public AppImageRepository(UserContext context)
        {
            _context = context;
        }

        public async Task CreateAsync(AppImage image)
        {
            await _context.AppImages.AddAsync(image);
        }
       
        public void Delete(AppImage image)
        {
            _context.AppImages.Remove(image);
        }

        public async Task<List<AppImage>> GetAsync(Expression<Func<AppImage, bool>> expression)
        {

            return await _context.AppImages.Where(expression).ToListAsync();
        }

        public async Task<AppImage> GetFirstOrDefaultAsync(Expression<Func<AppImage, bool>> expression)
        {
            return await _context.AppImages.FirstOrDefaultAsync(expression);
        }
        public AppImage GetFirstOrDefault(Expression<Func<AppImage, bool>> expression)
        {
            return _context.AppImages.FirstOrDefault(expression);
        }
        public IQueryable<AppImage> Get(Expression<Func<AppImage, bool>> expression)
        {
            return _context.AppImages.Where(expression);
        }

        public void Create(AppImage entity)
        {
            _context.AppImages.Add(entity);
        }

        public void DeleteRange(List<AppImage> images)
        {
            _context.RemoveRange(images);

        }
    }
}
