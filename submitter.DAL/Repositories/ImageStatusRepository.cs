﻿using Microsoft.EntityFrameworkCore;
using submitter.DAL.Models;
using submitter.DAL.Repositories.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace submitter.DAL.Repositories
{
    public class ImageStatusRepository : IImageStatusRepository
    {
        private readonly UserContext _context;

        public ImageStatusRepository(UserContext context)
        {
            _context = context;
        }
        public async Task<List<ImageStatus>> GetAsync(Expression<Func<ImageStatus, bool>> expression)
        {
            return await _context.ImageStatuses.Where(expression).ToListAsync();
        }
        public IQueryable<ImageStatus> Get(Expression<Func<ImageStatus, bool>> expression)
        {
            return _context.ImageStatuses.Where(expression);
        }
        public ImageStatus GetFirstOrDefault(Expression<Func<ImageStatus, bool>> expression)
        {
            return _context.ImageStatuses.FirstOrDefault(expression);
        }

        public async Task<ImageStatus> GetFirstOrDefaultAsync(Expression<Func<ImageStatus, bool>> expression)
        {
            return await _context.ImageStatuses.FirstOrDefaultAsync(expression);
        }

        public async Task CreateAsync(ImageStatus entity)
        {
            await _context.ImageStatuses.AddAsync(entity);
        }

        public void Create(ImageStatus entity)
        {
            _context.ImageStatuses.Add(entity);
        }

        public void Delete(ImageStatus entity)
        {
            _context.ImageStatuses.Remove(entity);
        }
    }
}
