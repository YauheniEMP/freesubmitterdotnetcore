﻿using Microsoft.EntityFrameworkCore;
using submitter.DAL.Models;
using submitter.DAL.Repositories.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace submitter.DAL.Repositories
{
    public class StockDataRepository : IStockDataRepository
    {
        private readonly UserContext _context;
        public StockDataRepository(UserContext context)
        {
            _context = context;
        }

        public async Task<List<StockData>> GetAsync(Expression<Func<StockData, bool>> expression)
        {
            return await _context.StocksData.Where(expression).ToListAsync();
        }

        public IQueryable<StockData> Get(Expression<Func<StockData, bool>> expression)
        {
            return _context.StocksData.Where(expression);
        }

        public async Task CreateAsync(StockData data)
        {
            await _context.StocksData.AddAsync(data);
        }

        public async Task<StockData> GetFirstOrDefaultAsync(Expression<Func<StockData, bool>> expression)
        {
            return await _context.StocksData.FirstOrDefaultAsync(expression);
        }

        public StockData GetFirstOrDefault(Expression<Func<StockData, bool>> expression)
        {
            return _context.StocksData.FirstOrDefault(expression);
        }

        public void Create(StockData entity)
        {
            _context.StocksData.Add(entity);
        }

        public void Delete(StockData entity)
        {
            _context.StocksData.Remove(entity);
        }
    }
}
