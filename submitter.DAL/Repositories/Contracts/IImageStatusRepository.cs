﻿using submitter.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace submitter.DAL.Repositories.Contracts
{
    public interface IImageStatusRepository:IRepository<ImageStatus>
    {
    }
}
