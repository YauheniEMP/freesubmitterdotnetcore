﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace submitter.DAL.Repositories.Contracts
{
    public interface IRepository<T> where T: class
    {
        IQueryable<T> Get(Expression<Func<T, bool>> expression);
        Task<List<T>> GetAsync(Expression<Func<T, bool>> expression);
        Task<T> GetFirstOrDefaultAsync(Expression<Func<T, bool>> expression);
        T GetFirstOrDefault(Expression<Func<T, bool>> expression);
        Task CreateAsync(T entity);
        void Create(T entity);
        void Delete(T entity);
    }
}
