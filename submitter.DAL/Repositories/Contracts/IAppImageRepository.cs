﻿using submitter.DAL.Models;
using System.Collections.Generic;

namespace submitter.DAL.Repositories.Contracts
{
    public interface IAppImageRepository:IRepository<AppImage>
    {
        void DeleteRange(List<AppImage> images);
    }
}
