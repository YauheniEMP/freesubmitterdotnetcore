﻿using submitter.DAL.Models;

namespace submitter.DAL.Repositories.Contracts
{
    public interface IStockDataRepository:IRepository<StockData>
    {
    }
}
