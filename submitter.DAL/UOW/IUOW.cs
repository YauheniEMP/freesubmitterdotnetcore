﻿using submitter.DAL.Repositories.Contracts;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace submitter.DAL.UOW
{
    public interface IUOW:IDisposable
    {
        IAppImageRepository AppImageRepository { get; }
        IStockDataRepository StockDataRepository { get; }
        IImageStatusRepository ImageStatusRepository { get; }
        Task SaveAsync();
        void Save();
    }
}
