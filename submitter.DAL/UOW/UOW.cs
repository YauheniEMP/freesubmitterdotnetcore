﻿using Microsoft.EntityFrameworkCore;
using submitter.DAL.Models;
using submitter.DAL.Repositories;
using submitter.DAL.Repositories.Contracts;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace submitter.DAL.UOW
{
    public class UOW : IUOW
    {
        private readonly UserContext _context;
        public IAppImageRepository AppImageRepository { get; }
        public IStockDataRepository StockDataRepository { get; }
        public IImageStatusRepository ImageStatusRepository { get; }
        public UOW(UserContext context)
        {
            _context = context;
            AppImageRepository = new AppImageRepository(context);
            StockDataRepository = new StockDataRepository(context);
            ImageStatusRepository = new ImageStatusRepository(context);
        }

        public async Task SaveAsync()
        {
            await _context.SaveChangesAsync();
        }
        public void Save()
        {
            _context.SaveChanges();
        }

        public void Dispose()
        {
            if (_context != null)
            {
                _context.Dispose();
                GC.SuppressFinalize(this);
            }
        }
    }
}
