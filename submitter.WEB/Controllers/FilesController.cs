﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using submitter.BLL.Models;
using submitter.BLL.Services.Contracts;

namespace submitter.WEB.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class FilesController : ControllerBase
    {
        private readonly IFilesService _filesService;
        private readonly ISubmitService _submitService;
        public FilesController(IFilesService filesService, ISubmitService submitService)
        {
            _filesService = filesService;
            _submitService = submitService;
        }

        [HttpGet]
        [Route("GetProcessingFiles")]
        [DisableRequestSizeLimit]
        public async Task<IActionResult> GetProcessingFiles()
        {
            return Ok(new { images = await _filesService.GetProcessingFiles() });
        }

        [Route("DeleteImages")]
        [HttpPost]
        public async Task<IActionResult> DeleteImages(DeleteImagesModel model)
        {
            return Ok(new { result = await _filesService.DeleteFilesAsync(model.Names) });
        }

        [Route("SubmitImages")]
        [HttpPost]
        public async Task<IActionResult> SubmitImages(string[] fileNames)
        {
            await _submitService.SubmitImagesAsync(fileNames);
            return Ok();
        }

        [Route("SubmitImage")]
        [HttpPost]
        public async Task<IActionResult> SubmitImage(SubmitImageModel model)
        {
            var result = await _submitService.SubmitImageAsync(model.FileName, model.Stock);
            return Ok();
        }

        [Route("GetLastSubmittedImages")]
        [HttpGet]
        public async Task<IActionResult> GetLastSubmittedImages(int page = 1)
        {
            return Ok( await _filesService.GetLastSubmittedFiles(page) );
        }
        [Route("GetNotSubmittedImages")]
        [HttpGet]
        public async Task<IActionResult> GetNotSubmittedImages()
        {
            return Ok(new { files = await _filesService.GetNotSubmittedImages() });
        }
    }
}