﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using submitter.BLL.Models;
using submitter.BLL.Services.Contracts;

namespace submitter.WEB.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AccountsController : ControllerBase
    {
        private readonly IAccountsService _accountsService;
        public AccountsController(IAccountsService accountsService)
        {
            _accountsService = accountsService;
        }

        [Route("Register")]
        [HttpPost]
        public async Task<IActionResult> Register([FromBody]RegisterModel model)
        {
            if (!ModelState.IsValid)
            {
                return Ok(new { result = false });
            }

            var result = await _accountsService.RegisterUser(model.Email, model.Password);
            return Ok(new { result = result.Result, error = result.ErrorMessage });
        }

        [Route("RecoveryPasswordEmail")]
        [HttpPost]
        public async Task<IActionResult> RecoveryPasswordEmail(RecoveryModel model)
        {
            var result = await _accountsService.SendRecoveryPasswordEmail(model.Email);
            return Ok(new { result });
        }

        [Route("ResetPassword")]
        [HttpPost]
        public async Task<IActionResult> ResetPassword(ResetPasswordModel model)
        {
            var result = await _accountsService.ResetPassword(model.Email, model.Code, model.Password, model.ConfirmPassword);
            return Ok(new { result });
        }

        [Route("ConfirmEmail")]
        [HttpPost]
        public async Task<IActionResult> ConfirmEmail(ConfirmModel model)
        {
            var result = await _accountsService.ConfirmEmailAsync(model.Email, model.Code);
            return Ok(new { result });
        }

        [Route("Login")]
        [HttpPost]
        public async Task<IActionResult> Login(LoginModel model)
        {
            if (model.Email == null && model.Password == null)
            {
                return Ok(new { result = false, isConfirmed = false });
            }
            var result = await _accountsService.LoginUser(model.Email, model.Password);
            bool isConfirmed = false;
            if (result)
            {
                isConfirmed = await _accountsService.IsEmailConfirmed(model.Email);
            }
            return Ok(new { result, isConfirmed });
        }

        

        [Route("Token")]
        [HttpGet]
        public async Task<IActionResult> Token()
        {
            var userName = HttpContext.User.Identity.Name;
            if (userName == null)
            {
                return Ok(new { Email = "default" });
            }
            var confirmed = await _accountsService.IsEmailConfirmed();
            return Ok(new { Email = userName, EmailConfirmed = confirmed });
        }

        [Route("Logout")]
        [HttpGet]
        public async Task<IActionResult> Logout()
        {
            await _accountsService.LogoutUser();
            return Ok();
        }
    }
}