﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using submitter.BLL.Models;
using submitter.BLL.Services.Contracts;

namespace submitter.WEB.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ImageDataController : ControllerBase
    {
        private readonly IImageDataService _imageDataService;

        public ImageDataController(IImageDataService imageDataService)
        {
            _imageDataService = imageDataService;
        }

        [HttpGet]
        [Route("GetTitle/{fileName}")]
        public async Task<IActionResult> GetTitle(string fileName)
        {
            return Ok(new { title = await _imageDataService.GetTitle(fileName) });
        }

        [HttpPost]
        [Route("SetKeywords")]
        public async Task<IActionResult> SetKeywords(SetKeywordsModel model)
        {
            await _imageDataService.SetKeywords(model.FileName, model.Keywords);
            return Ok();
        }

        [HttpPost]
        [Route("SetTitle")]
        public async Task<IActionResult> SetTitle(SetTitleModel model)
        {
            await _imageDataService.SetTitle(model.FileName, model.Title);
            return Ok();
        }

        [HttpGet]
        [Route("GetKeywords/{fileName}")]
        public async Task<IActionResult> GetKeywords(string fileName)
        {
            return Ok(new { keywords = await _imageDataService.GetKeywords(fileName) });
        }

        [HttpGet]
        [Route("GetPreview")]
        public IActionResult GetPreview(string image)
        {
            var previewpath = _imageDataService.GetPreviewPath(image);

            if (!System.IO.File.Exists(previewpath))
            {
                return BadRequest();
            };

            return PhysicalFile(previewpath, "img/jpg");
        }

        [Route("UpdateImage")]
        [HttpPost]
        public async Task<IActionResult> UpdateImage(UpdateFilesModel model)
        {
            await _imageDataService.UpdateImageDataAsync(model.Items);
            return Ok(new { result = true });
        }
    }
}