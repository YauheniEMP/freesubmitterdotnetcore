﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using submitter.BLL.Models;
using submitter.BLL.Services.Contracts;

namespace submitter.WEB.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class StockDataController : ControllerBase
    {
        private readonly IStockDataService _stockDataService;
        public StockDataController(IStockDataService stockDataService)
        {
            _stockDataService = stockDataService;
        }

        [Route("StockData")]
        [HttpGet]
        public async Task<IActionResult> StockData()
        {
            var data = await _stockDataService.GetStockData();
            return Ok(new { data });
        }

        [Route("StockData")]
        [HttpPost]
        public async Task<IActionResult> StockData(List<StockDataModel> data)
        {
            var result = await _stockDataService.SaveStockData(data);
            return Ok(result);
        }

        [Route("StockItem")]
        [HttpPost]
        public async Task<IActionResult> StockItem(StockDataModel data)
        {
            var result = await _stockDataService.SaveStockItem(data);
            return Ok(result);
        }
    }
}