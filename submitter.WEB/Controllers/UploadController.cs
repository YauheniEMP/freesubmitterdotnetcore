﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using submitter.BLL.Services.Contracts;

namespace submitter.WEB.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class UploadController : ControllerBase
    {
        private readonly IUploadService _uploadService;

        public UploadController(IUploadService uploadService)
        {
            _uploadService = uploadService;
        }

        [HttpPost]
        [Route("UploadFile")]
        [DisableRequestSizeLimit]
        public async Task<IActionResult> UploadFile()
        {
            var file = HttpContext.Request.Form.Files[0];
            
            var result = await _uploadService.UploadFileAsync(file);

            if (!result.Succeded)
                return Ok(new { error = result.Error });
            else return Ok();
        }
    }
}