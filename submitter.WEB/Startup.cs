﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using submitter.BLL.Extensions;
using submitter.BLL.Modules;
namespace submitter.WEB
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }
        
        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            DataAccessModule.Load(services, Configuration);
            ServicesModule.Load(services);
            SettingsModule.Load(services);
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.PipeLine(env, Configuration);
        }
    }
}
