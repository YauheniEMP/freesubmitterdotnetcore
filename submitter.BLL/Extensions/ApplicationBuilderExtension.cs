﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using submitter.DAL.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Hosting;

using System;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.DataProtection;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using submitter.BLL.Modules;

namespace submitter.BLL.Extensions
{
    public static class ApplicationBuilderExtension
    {
        public static void Migrate(this IApplicationBuilder builder)
        {
            using (var serviceScope = builder.ApplicationServices.GetRequiredService<IServiceScopeFactory>().CreateScope())
            {
                var context = serviceScope.ServiceProvider.GetService<UserContext>();
                context.Database.Migrate();
            }
        }

        public static void PipeLine(this IApplicationBuilder app, IHostingEnvironment env, IConfiguration configuration)
        {
            //app.Migrate();

            if (env.IsDevelopment())
            {

                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }
            app.UseCors(b => b.WithOrigins(configuration.GetValue<string>("BaseURI")).AllowAnyHeader().AllowAnyMethod().AllowCredentials());
            app.UseAuthentication();
            //app.UseHttpsRedirection();
            app.UseMvc();
        }
    }
}
