﻿using System.Collections.Generic;

namespace submitter.BLL.Extensions
{
    public static class StringExtension
    {
        public static string[] GetArrayFromString(this string str)
        {
            if (str.Length == 0)
            {
                return null;
            }
            var array = str.Split(' ');
            List<string> newList = new List<string>();
            foreach (var elem in array)
            {
                if (!string.IsNullOrEmpty(elem))
                {
                    newList.Add(elem);
                }
            }

            return newList.ToArray();
        }

        public static string SetEpsExtension(this string str)
        {
            return str.Substring(0, str.Length - 4) + ".eps";
        }

        public static string RemoveExtension(this string str)
        {
            return str.Substring(0, str.Length - 4);
        }

        public static string AddEpsExtension(this string str)
        {
            return str + ".eps";
        }
        public static string SetEpsOriginalExtension(this string str)
        {
            return str.Substring(0, str.Length - 4) + ".eps_original";
        }

        public static string SetJpgExtension(this string str)
        {
            return str.Substring(0, str.Length - 4) + ".jpg";
        }

        public static string SetJpgOriginalExtension(this string str)
        {
            return str.Substring(0, str.Length - 4) + ".jpg_original";
        }

        public static string SetZipExtension(this string str)
        {
            return str.Substring(0, str.Length - 4) + ".zip";
        }
    }
}
