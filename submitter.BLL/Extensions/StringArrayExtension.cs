﻿namespace submitter.BLL.Extensions
{
    public static class StringArrayExtension
    {
        public static string GetStringFromArray(this string[] array)
        {
            string result = "";
            if (array != null)
            {
                foreach (var val in array)
                {
                    result += val + " ";
                }
            }
            return result;
        }
    }
}
