﻿using Microsoft.AspNetCore.DataProtection;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;

namespace submitter.BLL.Modules
{
    public static class SettingsModule
    {
        public static void Load(IServiceCollection services)
        {
            services.AddDataProtection()
               .SetDefaultKeyLifetime(TimeSpan.FromDays(90));
            services.ConfigureApplicationCookie(p =>
            {
                p.Cookie.Expiration = TimeSpan.FromDays(90);
            });
            services.AddCors();
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
        }
    }
}
