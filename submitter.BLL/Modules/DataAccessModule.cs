﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using submitter.DAL.Models;
using submitter.DAL.Repositories;
using submitter.DAL.Repositories.Contracts;
using submitter.DAL.UOW;

namespace submitter.BLL.Modules
{
    public static class DataAccessModule
    {
        public static void Load(IServiceCollection services, IConfiguration configuration)
        {
            services.AddScoped<IUOW, UOW>();
            services.AddScoped<IAppImageRepository, AppImageRepository>();
            services.AddScoped<IStockDataRepository, StockDataRepository>();
            services.AddScoped<IImageStatusRepository, ImageStatusRepository>();

            services.AddDbContext<UserContext>(opt => opt.UseSqlServer(configuration.GetConnectionString("dbStockUsers")));
            services.AddIdentity<User, IdentityRole>(b =>
            {
                b.Password.RequireDigit = false;
                b.Password.RequiredLength = 4;
                b.Password.RequireNonAlphanumeric = false;
                b.Password.RequireUppercase = false;
                b.Password.RequireLowercase = false;
            }
            )
                .AddEntityFrameworkStores<UserContext>()
                .AddDefaultTokenProviders()
                .AddDefaultUI();
        }
    }
}
