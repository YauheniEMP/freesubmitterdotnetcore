﻿using Microsoft.Extensions.DependencyInjection;
using submitter.BLL.Services;
using submitter.BLL.Services.Contracts;
using System;
using System.Collections.Generic;
using System.Text;

namespace submitter.BLL.Modules
{
    public class ServicesModule
    {
        public static void Load(IServiceCollection services)
        {
            services.AddSingleton<IConnectionsHandlerService, ConnectionsHandlerService>();
            services.AddTransient<IConnectionProviderService, ConnectionProviderService>();
            services.AddTransient<IUploadResultService, UploadResultService>();
            services.AddTransient<IUploadToStockService, UploadToStockService>();
            services.AddTransient<ISubmitService, SubmitService>();
            services.AddTransient<IImageDataService, ImageDataService>();
            services.AddTransient<IUploadService, UploadService>();
            services.AddTransient<ICryptService, LocalCryptService>();
            services.AddTransient<IAccountsService, AccountsService>();
            services.AddTransient<IEmailService, EmailService>();
            services.AddTransient<IUserService, UserService>();
            services.AddTransient<IMetadataService, MetadataService>();
            services.AddTransient<IFilesService, FilesService>();
            services.AddTransient<IDirectoryService, DirectoryService>();
            services.AddTransient<IStockDataService, StockDataService>();
        }
    }
}
