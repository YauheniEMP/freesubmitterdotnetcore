﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace submitter.BLL.DTO
{
    public class ImageStatusDTO
    {
        public string Stock { get; set; }
        public string Status { get; set; }
    }
}
