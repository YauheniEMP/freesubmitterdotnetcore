﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace submitter.BLL.DTO
{
    public class HistoryImageDTO
    {
        public string Name { get; set; }
       
        public List<ImageStatusDTO> StatusList { get; set; }
        public DateTime SubmitDateTime { get; set; }
        public bool IsSubmited { get; set; }
    }
}
