﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace submitter.BLL.DTO
{
    public class StockDataDTO
    {
        public string SSLogin { get; set; }
        public string SSPass { get; set; }
        public bool SSPermission { get; set; }
        public string GILogin { get; set; }
        public string GIPass { get; set; }
        public bool GIPermission { get; set; }
        public string ASLogin { get; set; }
        public string ASPass { get; set; }
        public bool ASPermission { get; set; }
        public string DTLogin { get; set; }
        public string DTPass { get; set; }
        public bool DTPermission { get; set; }
        public string BSLogin { get; set; }
        public string BSPass { get; set; }
        public bool BSPermission { get; set; }
        public string DPLogin { get; set; }
        public string DPPass { get; set; }
        public bool DPPermission { get; set; }
        public string RFLogin { get; set; }
        public string RFPass { get; set; }
        public bool RFPermission { get; set; }
        public string VSLogin { get; set; }
        public string VSPass { get; set; }
        public bool VSPermission { get; set; }
    }
}
