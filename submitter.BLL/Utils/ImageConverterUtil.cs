﻿using ImageMagick;
using System;
using submitter.BLL.Models;
using System.IO;
using submitter.BLL.Extensions;

namespace submitter.BLL.Utils
{
    public static class ImageConverterUtil
    {
        public static ResultModel GenerateJpgFiles(string fileName, string rootFolder, string watermarkPath, string ghostScriptDirectory)
        {
            var epsFilePath = rootFolder + fileName.SetEpsExtension();
            var jpgFilePath = rootFolder + fileName.SetJpgExtension();
            var prwFilePath = rootFolder + "PRW" + "//" + fileName.SetJpgExtension();
            var temp = rootFolder + "TEMP" + "//";
            try
            {
                if (!Directory.Exists(temp))
                {
                    Directory.CreateDirectory(temp);
                }
                MagickNET.SetGhostscriptDirectory(ghostScriptDirectory);
                MagickNET.SetTempDirectory(temp);
                using (MagickImage image = new MagickImage(epsFilePath, new MagickReadSettings { ColorSpace = ColorSpace.sRGB }))
                {
                    int newWidth;
                    int newHeigth;

                    var minsize = image.BaseWidth < image.BaseHeight ? image.BaseWidth : image.BaseHeight;

                    if (minsize == image.BaseWidth)
                    {
                        newWidth = 5000;
                        newHeigth = 5000 * image.BaseHeight / image.BaseWidth;
                    }
                    else
                    {
                        newHeigth = 5000;
                        newWidth = 5000 * image.BaseWidth / image.BaseHeight;
                    }
                    image.Resize(newWidth, newHeigth);

                    image.AddProfile(ColorProfile.AdobeRGB1998);
                    image.AddProfile(ColorProfile.SRGB);
                    image.ColorSpace = ColorSpace.RGB;
                    image.Write(jpgFilePath);
                    image.Scale(new Percentage(5));
                    var maxsizeprw = image.Height > image.Width ? image.Height : image.Width;
                    image.Extent(maxsizeprw, maxsizeprw, Gravity.Center, new MagickColor("white"));
                    using (MagickImage watermark = new MagickImage(watermarkPath))
                    {
                        image.Composite(watermark, Gravity.Center, CompositeOperator.Multiply);
                    }
                    image.Write(prwFilePath);
                }
                return new ResultModel { Result = true };
            }
            catch (Exception ex)
            {
                return new ResultModel { Result = false, ErrorMessage = ex.Message };
            }
        }
    }
}
