﻿using submitter.BLL.Extensions;
using System.IO;
using System.IO.Compression;

namespace submitter.BLL.Utils
{
    public static class PackUtil
    {
        public static void PackFile(string fileName, string rootFolder)
        {
            if (File.Exists(rootFolder + fileName.SetZipExtension()))
            {
                File.Delete(rootFolder + fileName.SetZipExtension());
            }
            using (ZipArchive archive = ZipFile.Open(rootFolder + fileName.SetZipExtension(), ZipArchiveMode.Create))
            {
                archive.CreateEntryFromFile(rootFolder + fileName, fileName);
                archive.CreateEntryFromFile(rootFolder + fileName.SetJpgExtension(), fileName.SetJpgExtension());
            }
        }
    }
}
