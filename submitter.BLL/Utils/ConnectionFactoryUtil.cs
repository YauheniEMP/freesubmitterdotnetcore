﻿using submitter.BLL.Models.ConnectionModels;
using submitter.BLL.Models.ConnectionModels.Contracts;
using submitter.DAL.Enums;
using submitter.DAL.Models;
using System.Net;

namespace submitter.BLL.Utils
{
    public static class ConnectionFactoryUtil
    {
        public static IConnection CreateConnection(StockData data, UploadType uploadType)
        {
            if (uploadType == UploadType.Ftp)
            {
                return new FtpConnection
                {
                    Data = data,
                    UploadType = uploadType,
                    Client = new WebClient()
                    {
                        Credentials = new NetworkCredential(data.Login, data.Password)
                    }
                };
            }
            return null;
        }
    }
}
