﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace submitter.BLL.Utils
{
    public static class OriginalsUtil
    {
        public static void RemoveOriginals(string rootDirectory)
        {
            if (Directory.Exists(rootDirectory))
            {
                var dir = new DirectoryInfo(rootDirectory);
                var files = dir.GetFiles().Where(p => p.FullName.EndsWith('l'));
                if (files.Count() != 0)
                {
                    foreach (var file in files)
                    {
                        file.Delete();
                    }
                }
            }
        }
    }
}
