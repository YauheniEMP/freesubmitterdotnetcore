﻿using submitter.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace submitter.BLL.Models
{
    public class UploadStockResult
    {
        public string Name { get; set; }
        public StocksList Stock { get; set; }
        public bool Result { get; set; }
    }
}
