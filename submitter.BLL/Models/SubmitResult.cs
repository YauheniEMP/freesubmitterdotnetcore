﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace submitter.BLL.Models
{
    public class SubmitResult
    {
        public bool Result { get; set; }
        public bool UploadedToShutterstock { get; set; }
        public bool UploadedToAdobestock { get; set; }
        public bool UploadedToGettyImages { get; set; }
        public bool UploadedToBigstock { get; set; }
        public bool UploadedToDepositphotos { get; set; }
        public bool UploadedToVectorstock { get; set; }
        public bool UploadedToRoyaltyFree { get; set; }
        public bool UploadedToDreamstime { get; set; }
    }
}
