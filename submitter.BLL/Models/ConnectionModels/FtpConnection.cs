﻿using submitter.BLL.Models.ConnectionModels.Contracts;
using submitter.DAL;
using submitter.DAL.Enums;
using submitter.DAL.Models;
using System;
using System.Collections.Generic;
using System.Net;
using submitter.BLL.Services.Contracts;
using System.Threading.Tasks;
using submitter.BLL.Extensions;

namespace submitter.BLL.Models.ConnectionModels
{
    public class FtpConnection : IConnection,IDisposable
    {
        public UploadType UploadType { get; set; }
        public StockData Data { get; set; }
        public WebClient Client { get; set; }
        public bool IsInProcess { get;  set; }

        private Queue<string> Files { get; set; } = new Queue<string>();
        private string _rootImagesPath { get; set; }
        private IUploadResultService uploadResultEmitter;

        
        public IConnection AddEmmitter(IUploadResultService resultService)
        {
            uploadResultEmitter = resultService;
            return this;
        }

        public IConnection AddRootPath(string rootImagesPath)
        {
            _rootImagesPath = rootImagesPath;
            return this;
        }
        public void AddFiles(string[] files)
        {
            if (files == null || files.Length == 0)
            {
                return;
            }
            foreach (var file in files)
            {
                Files.Enqueue(file);
            }
        }

        public bool StartUpload(string rootPath)
        {
            if (!IsInProcess)
            {
                IsInProcess = true;
                _rootImagesPath = rootPath;
                while (Files.Count != 0)
                {
                    var file = Files.Dequeue();
                    var result = PassToUpload(file);
                    if (result)
                    {
                        uploadResultEmitter.SetResult(Data, file, StatusList.Submitted);
                    }
                    else
                    {
                        uploadResultEmitter.SetResult(Data, file, StatusList.Failed);
                    }
                    
                }
                if (Client != null)
                {
                    Client.Dispose();
                }
                return true;
            }
            
            return false;
        }
       
        private bool PassToUpload(string file)
        {
            if (Data.Stock == StocksList.AdobeStock)
            {
                var localFilePath = _rootImagesPath +file.SetZipExtension();
                Uri uri = new Uri($"ftp://ftp.contributor.adobestock.com/{file.SetZipExtension()}");
                return StartUpload(uri, localFilePath);
            }

            if (Data.Stock == StocksList.ShutterStock)
            {
                var localFilePath = _rootImagesPath  + file;
                Uri uri = new Uri($"ftp://ftp.shutterstock.com/{file}");
                return StartUpload(uri, localFilePath);
            }

            if (Data.Stock == StocksList.Bigstock)
            {
                var localFilePath = _rootImagesPath + file;
                Uri uri = new Uri($"ftp://ftp.bigstockphoto.com/{file}");
                return StartUpload(uri, localFilePath);
            }
            if (Data.Stock == StocksList.RoyaltyFree)
            {
                var localFilePath = _rootImagesPath + file;
                var localFilePath1 = _rootImagesPath + file.SetJpgExtension();
                Uri uri = new Uri($"ftp://ftp.123rf.com/{file}");
                Uri uri1 = new Uri($"ftp://ftp.123rf.com/{file.SetJpgExtension()}");
                return StartUpload(uri, uri1, localFilePath, localFilePath1);
            }
            if (Data.Stock == StocksList.DreamsTime)
            {
                var localFilePath1 = _rootImagesPath +file;
                var localFilePath = _rootImagesPath + file.SetJpgExtension();
                Uri uri1 = new Uri($"ftp://upload.dreamstime.com/additional/{file}");
                Uri uri = new Uri($"ftp://upload.dreamstime.com/{file.SetJpgExtension()}");
                return StartUpload(uri, uri1, localFilePath, localFilePath1);
            }
            if (Data.Stock == StocksList.Depositphotos)
            {
                var localFilePath = _rootImagesPath + file.SetZipExtension();
                Uri uri = new Uri($"ftp://ftp.depositphotos.com/{file.SetZipExtension()}");
                return StartUpload(uri, localFilePath);
            }
            if (Data.Stock == StocksList.VectorStock)
            {
                var localFilePath = _rootImagesPath + file.SetZipExtension();
                Uri uri = new Uri($"ftp://batch.vectorstock.com/{file.SetZipExtension()}");
                return StartUpload(uri, localFilePath);
            }

            return false;
        }

        private bool StartUpload(Uri uri, string localFilePath)
        {
            var attempt = 1;
            while (attempt++ <= 5)
            {
                try
                {
                    Client.UploadFile(uri, WebRequestMethods.Ftp.UploadFile, localFilePath);
                    return true;
                }
                catch (Exception ex)
                {
                    if (uri.Host.Contains("depositphotos"))
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        private bool StartUpload(Uri uri, Uri uri1, string localFilePath, string localFilePath1)
        {
            var attempt = 1;
            while (attempt++ <= 5)
            {
                try
                {
                    Client.UploadFile(uri, WebRequestMethods.Ftp.UploadFile, localFilePath);
                    if (uri.Host.Contains("dreams"))
                    {
                        Task.Factory.StartNew(() => Task.Delay(120000).Wait()).Wait();
                    }
                    Client.UploadFile(uri1, WebRequestMethods.Ftp.UploadFile, localFilePath1);
                    return true;
                }
                catch (Exception ex)
                {

                }
            }
            return false;
        }

        public void Dispose()
        {
            if (Client != null)
            {
                Client.Dispose();
            }
        }
    }
}
