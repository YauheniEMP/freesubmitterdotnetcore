﻿using submitter.BLL.Services.Contracts;
using submitter.DAL;
using submitter.DAL.Enums;
using submitter.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace submitter.BLL.Models.ConnectionModels.Contracts
{
    public interface IConnection:IDisposable
    {
        UploadType UploadType { get; set; }
        StockData Data { get; set; }
        void AddFiles(string[] files);
        bool StartUpload(string rootPath);
        bool IsInProcess { get;  set; }
        IConnection AddEmmitter(IUploadResultService resultService);
        IConnection AddRootPath(string rootImagesPath);
    }
}
