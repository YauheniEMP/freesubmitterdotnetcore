﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace submitter.BLL.Models
{
    public class StockDataModel
    {
        public string Stock { get; set; }
        public bool Permission { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }

    }
}
