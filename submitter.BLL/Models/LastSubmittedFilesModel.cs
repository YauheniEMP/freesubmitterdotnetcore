﻿using submitter.BLL.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace submitter.BLL.Models
{
    public class LastSubmittedFilesModel
    {
        public List<HistoryImageDTO> Images { get; set; }
        public int Pages { get; set; }
    }
}
