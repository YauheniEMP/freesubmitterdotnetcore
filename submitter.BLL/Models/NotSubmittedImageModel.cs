﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace submitter.BLL.Models
{
    public class NotSubmittedImageModel
    {
        public string Name { get; set; }
        public string Title { get; set; }
        public string[] Keywords { get; set; }

    }
}
