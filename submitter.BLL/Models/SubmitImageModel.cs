﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace submitter.BLL.Models
{
    public class SubmitImageModel
    {
        public string FileName { get; set; }
        public string Stock { get; set; }
    }
}
