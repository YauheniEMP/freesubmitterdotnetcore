﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace submitter.BLL.Models
{
    public class SubmitFileModel
    {
        public string FileName { get; set; }
    }
}
