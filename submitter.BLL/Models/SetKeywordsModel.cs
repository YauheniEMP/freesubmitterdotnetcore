﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace submitter.BLL.Models
{
    public class SetKeywordsModel
    {
        public string FileName { get; set; }
        public string[] Keywords { get; set; }
    }
}
