﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace submitter.BLL.Models
{
    public class ResultModel
    {
        public bool Result { get; set; }
        public string ErrorMessage { get; set; }
    }
}
