﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace submitter.BLL.Models
{
    public class UploadResult
    {
        public bool Succeded { get; set; }
        public string Error { get; set; }
    }
}
