﻿using submitter.BLL.Models;
using submitter.BLL.Services.Contracts;
using submitter.DAL.UOW;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using submitter.BLL.DTO;
using submitter.DAL;
using submitter.DAL.Enums;
using submitter.BLL.Extensions;
using submitter.DAL.Models;

namespace submitter.BLL.Services
{
    public class FilesService : IFilesService
    {
        private readonly IUOW _uow;
        private readonly IMetadataService _metadataService;
        private readonly IDirectoryService _directoryService;
        private readonly IUserService _userService;
        private readonly ICryptService _cryptService;
        private string UserId { get; }

        public FilesService(
            IUOW uow, IMetadataService metadataService, IDirectoryService directoryService,
            IUserService userService, ICryptService cryptService)
        {
            _userService = userService;
            _uow = uow;
            _metadataService = metadataService;
            _directoryService = directoryService;
            _cryptService = cryptService;
            UserId = _userService.GetCurrentUserId();
        }

        public async Task<bool> DeleteFilesAsync(string[] fileNames)
        {
            var list = new List<AppImage>();
            foreach (var fileName in fileNames)
            {
                list.Add(await _uow.AppImageRepository.GetFirstOrDefaultAsync(p => p.Name == fileName.SetEpsExtension() && p.UserId == UserId));
            }
            _uow.AppImageRepository.DeleteRange(list);
            await _uow.SaveAsync();
            foreach (var fileName in fileNames)
            {
                DeleteFile(fileName);
            }
            return true;
        }
        private  void DeleteFile(string fileName)
        {
            var fileJpgFullPath = _directoryService.RootFolder + fileName.SetJpgExtension();
            var fileEpsFullPath = _directoryService.RootFolder + fileName.SetEpsExtension();
            var filePrwFullPath = _directoryService.PreviewFolder + fileName.SetJpgExtension();
            var fileZipFullPath = _directoryService.RootFolder + fileName.SetZipExtension();

            if (File.Exists(fileJpgFullPath))
            {
                File.Delete(fileJpgFullPath);
            }
            if (File.Exists(fileZipFullPath))
            {
                File.Delete(fileZipFullPath);
            }
            if (File.Exists(fileEpsFullPath))
            {
                File.Delete(fileEpsFullPath);
            }
            if (File.Exists(filePrwFullPath))
            {
                File.Delete(filePrwFullPath);
            }
        }
        
        public async Task<LastSubmittedFilesModel> GetLastSubmittedFiles(int page)
        {

            var lastFiles = _uow.AppImageRepository.Get(p => p.UserId == UserId && p.IsSubmitCompleted == true);
            var pages = (lastFiles.Count()-1)/50+1;

            var filesDTO = new List<HistoryImageDTO>();
            foreach (var file in lastFiles.OrderByDescending(p => p.SubmitDateTime).Skip(page*50-50).Take(page*50).ToList())
            {
                var imageStatus = await _uow.ImageStatusRepository.GetAsync(p => p.AppImageId == file.Id);
                var listDTO = new List<ImageStatusDTO>();
                foreach (var item in imageStatus)
                {
                    listDTO.Add(new ImageStatusDTO
                    {
                        Status = Enum.GetName(typeof(StatusList), item.Status),
                        Stock = Enum.GetName(typeof(StocksList), item.Stock)
                    });
                }
                filesDTO.Add(new HistoryImageDTO
                {
                    IsSubmited = file.IsSubmitted,
                    Name = file.Name,
                    SubmitDateTime = file.SubmitDateTime,
                    StatusList = listDTO.OrderBy(p => p.Stock).ToList()
                });
            }
            return new LastSubmittedFilesModel { Images = filesDTO.ToList(), Pages = pages };
        }

        public async Task<List<HistoryImageDTO>> GetProcessingFiles()
        {
            var lastFiles = await _uow.AppImageRepository.GetAsync(p => p.UserId == UserId && p.IsSubmitCompleted == false && p.IsSubmitted == true );
            var filesDTO = new List<HistoryImageDTO>();
            foreach (var file in lastFiles.Take(100))
            {
                var imageStatus = await _uow.ImageStatusRepository.GetAsync(p => p.AppImageId == file.Id);
                var listDTO = new List<ImageStatusDTO>();
                foreach (var item in imageStatus)
                {
                    listDTO.Add(new ImageStatusDTO
                    {
                        Status = Enum.GetName(typeof(StatusList), item.Status),
                        Stock = Enum.GetName(typeof(StocksList), item.Stock)
                    });
                }
                filesDTO.Add(new HistoryImageDTO
                {
                    IsSubmited = file.IsSubmitted,
                    Name = file.Name,
                    SubmitDateTime = file.SubmitDateTime,
                    StatusList = listDTO.OrderBy(p => p.Stock).ToList()
                });
            }
            return filesDTO.OrderByDescending(p => p.SubmitDateTime).ToList();
        }

        public async Task<List<NotSubmittedImageModel>> GetNotSubmittedImages()
        {
            var files = await _uow.AppImageRepository.GetAsync(p => p.UserId == UserId && p.IsSubmitted == false);
            var list = new List<NotSubmittedImageModel>();
            if (files.Count != 0)
            {
                foreach (var file in files)
                {
                    list.Add(new NotSubmittedImageModel
                    {
                        Name = file.Name,
                        Keywords = file.Keywords.GetArrayFromString(),
                        Title = file.Title
                    });
                }
            }
            return list;
        }
    }
}
