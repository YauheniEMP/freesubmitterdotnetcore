﻿using MimeKit;
using MailKit.Net.Smtp;
using submitter.BLL.Services.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MailKit.Security;
using Microsoft.Extensions.Configuration;

namespace submitter.BLL.Services
{
    public class EmailService:IEmailService
    {
        private readonly string _baseURI ;
        public EmailService(IConfiguration configuration)
        {
            _baseURI = configuration.GetValue<string>("BaseURI");
        }

        public async Task SendEmailAsync(string email,string code)
        {
            var emailMessage = new MimeMessage();

            emailMessage.From.Add(new MailboxAddress("iSubmitter", "mighty.eug@gmail.com"));
            emailMessage.To.Add(new MailboxAddress("", email));
            emailMessage.Subject = "Confirm Email on site iSubmitter.net";
            var url = $"{_baseURI}/confirm?email={email}&code={code}";
            emailMessage.Body = new TextPart(MimeKit.Text.TextFormat.Html)
            {
                Text = $"Подтвердите регистрацию, перейдя по ссылке: <a href='{url}'>link</a>"
            };

            using (var client = new SmtpClient())
            {
                //await client.ConnectAsync("smtp.gmail.com", 25, false);
                await client.ConnectAsync("smtp.gmail.com", 587, SecureSocketOptions.StartTls);
                await client.AuthenticateAsync("mighty.eug@gmail.com", "5u67625u6762");
                await client.SendAsync(emailMessage);
            }
        }

        public async Task SendRecoveryPasswordEmailAsync(string email, string code)
        {
            var emailMessage = new MimeMessage();

            emailMessage.From.Add(new MailboxAddress("iSubmitter", "mighty.eug@gmail.com"));
            emailMessage.To.Add(new MailboxAddress("", email));
            emailMessage.Subject = "Recovery password on iSubmitter.net";
            var url = $"{_baseURI}/changepassword?email={email}&code={code}";
            emailMessage.Body = new TextPart(MimeKit.Text.TextFormat.Html)
            {
                Text = $"Для смены пароля перейдите по ссылке: <a href='{url}'>link</a>"
            };

            using (var client = new SmtpClient())
            {
                //await client.ConnectAsync("smtp.gmail.com", 25, false);
                await client.ConnectAsync("smtp.gmail.com", 587, SecureSocketOptions.StartTls);
                await client.AuthenticateAsync("mighty.eug@gmail.com", "5u67625u6762");
                await client.SendAsync(emailMessage);
            }
        }
    }
}
