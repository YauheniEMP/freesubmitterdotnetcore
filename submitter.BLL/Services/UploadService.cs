﻿using Microsoft.AspNetCore.Http;
using submitter.BLL.Models;
using submitter.BLL.Services.Contracts;
using submitter.DAL.Models;
using submitter.DAL.UOW;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using submitter.DAL;
using submitter.BLL.Utils;
using submitter.BLL.Extensions;

namespace submitter.BLL.Services
{
    public class UploadService : IUploadService
    {
        private readonly IDirectoryService _directoryService;
        private readonly IUOW _uow;
        private readonly IMetadataService _metadataService;
        private readonly string UserId;

        public UploadService(IDirectoryService directoryService, IUOW uow, IMetadataService metadataService,
            IUserService userService)
        {
            _directoryService = directoryService;
            _uow = uow;
            _metadataService = metadataService;
            UserId = userService.GetCurrentUserId();
        }

        public async Task<UploadResult> UploadFileAsync(IFormFile file)
        {
            var fileName = await SetNewFileName(file.FileName);

            if (!Directory.Exists(_directoryService.RootFolder))
            {
                Directory.CreateDirectory(_directoryService.RootFolder);
            }
            if (!Directory.Exists(_directoryService.PreviewFolder))
            {
                Directory.CreateDirectory(_directoryService.PreviewFolder);
            }

            try
            {
                await UploadEpsFile(file, fileName);
            }
            catch
            {
                return new UploadResult { Succeded = false, Error = "Upload eps file fails" };
            }
            var result = ImageConverterUtil.GenerateJpgFiles(fileName, _directoryService.RootFolder, _directoryService.WatermarkPath, _directoryService.GhostScriptPath);
            if (!result.Result)
            {
                return new UploadResult { Succeded = false, Error = result.ErrorMessage };
            }

            await AddNewImageToDatabase(fileName);

            return new UploadResult { Succeded = true };
        }

        private async Task<string> SetNewFileName(string oldName)
        {
            while (await _uow.AppImageRepository.GetFirstOrDefaultAsync(p => p.UserId == UserId && p.Name == oldName) != null)
            {
                oldName = GenerateNewName(oldName);
            }
            return oldName;
        }

        private string GenerateNewName(string name)
        {
            Random rnd = new Random();
            name = (name.RemoveExtension() + rnd.Next(1, 100).ToString()).AddEpsExtension();
            return name;
        }

        private async Task UploadEpsFile(IFormFile file, string fileName)
        {
            var epsFilePath = _directoryService.RootFolder + fileName.SetEpsExtension();
            using (var ms = new MemoryStream())
            {
                await file.CopyToAsync(ms);
                var fileBytes = ms.ToArray();

                using (var writer = new FileStream(epsFilePath, FileMode.Create))
                {
                    await writer.WriteAsync(fileBytes);
                    writer.Close();
                }
                ms.Close();
            }
        }

        private async Task AddNewImageToDatabase(string fileName)
        {
            var keywords = await _metadataService.GetKeywords(fileName);
            var list = new List<ImageStatus>();
            foreach (var stock in Enum.GetValues(typeof(StocksList)))
            {
                list.Add(new ImageStatus
                {
                    Stock = (StocksList)stock,
                    Status = DAL.Enums.StatusList.Uploaded
                });
            }
            await _uow.AppImageRepository.CreateAsync(
                new AppImage
                {
                    Name = fileName,
                    DateUpload = DateTime.Now,
                    Title = await _metadataService.GetTitle(fileName),
                    Keywords = keywords.GetStringFromArray(),
                    UserId = UserId,
                    ImageStatus = list
                });
            await _uow.SaveAsync();
        }
    }
}
