﻿using submitter.BLL.Services.Contracts;
using submitter.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace submitter.BLL.Services
{
    public class UploadToStockService:IUploadToStockService
    {
        private readonly IConnectionProviderService _connectionProvider;
        private readonly IDirectoryService _directoryService;
        public UploadToStockService(IConnectionProviderService connectionProvider, IDirectoryService directoryService)
        {
            _connectionProvider = connectionProvider;
            _directoryService = directoryService;
        }

        public void Start(StockData data, string[] fileNames)
        {
            var connection = _connectionProvider.GetConnection(data, DAL.Enums.UploadType.Ftp);
            var isActive = connection.IsInProcess;
            connection.AddFiles(fileNames);
            connection.StartUpload(_directoryService.RootFolder);
            if (!isActive)
            {
                _connectionProvider.RemoveConnection(connection);
            }
        }
    }
}
