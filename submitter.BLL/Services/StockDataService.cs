﻿using Microsoft.AspNetCore.Identity;
using submitter.BLL.Models;
using submitter.BLL.Services.Contracts;
using submitter.DAL;
using submitter.DAL.Models;
using submitter.DAL.UOW;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace submitter.BLL.Services
{
    public class StockDataService:IStockDataService
    {
        private readonly IUOW _uow;
        private readonly IUserService _userService;
        private readonly ICryptService _cryptService;
        private readonly UserManager<User> _userManager;
        public StockDataService(IUOW uow, IUserService userService, ICryptService cryptService, UserManager<User> userManager)
        {
            _uow = uow;
            _userService = userService;
            _cryptService = cryptService;
            _userManager = userManager;
        }

        public async Task<List<StockDataModel>> GetStockData()
        {
            var data = await _uow.StockDataRepository.GetAsync(p => p.UserId == _userService.GetCurrentUserId());
            var decryptedData = _cryptService.Decrypt(data);
            var list = new List<StockDataModel>();
            foreach (var item in decryptedData)
            {
                list.Add(new StockDataModel
                {
                    Login = item.Login,
                    Password = item.Password,
                    Permission = item.Permission,
                    Stock = Enum.GetName(typeof(StocksList), item.Stock)
                });
            }
            return list;
        }

        public async Task<bool> SaveStockData(List<StockDataModel> list)
        {
            var data = await _uow.StockDataRepository.GetAsync(p => p.UserId == _userService.GetCurrentUserId());
            foreach (var item in data)
            {
                item.Permission = list.FirstOrDefault(p => p.Stock == Enum.GetName(typeof(StocksList), item.Stock)).Permission;
                item.Login = _cryptService.Encrypt(list.FirstOrDefault(p => p.Stock == Enum.GetName(typeof(StocksList), item.Stock)).Login.Replace(" ", string.Empty));
                item.Password = _cryptService.Encrypt(list.FirstOrDefault(p => p.Stock == Enum.GetName(typeof(StocksList), item.Stock)).Password.Replace(" ", string.Empty));
            }
            await _uow.SaveAsync();
            return true;
        }

        public async Task<bool> SaveStockItem(StockDataModel item)
        {
            var data = await _uow.StockDataRepository.GetAsync(p => p.UserId == _userService.GetCurrentUserId());
            var itemData = data.FirstOrDefault(p => item.Stock == Enum.GetName(typeof(StocksList), p.Stock));

            itemData.Permission = item.Permission;
            itemData.Login = _cryptService.Encrypt(item.Login?.Replace(" ", string.Empty));
            itemData.Password = _cryptService.Encrypt(item.Password?.Replace(" ", string.Empty));

            await _uow.SaveAsync();
            return true;
        }

        public async Task<bool> CreateStockData(string email)
        {
            try
            {
                var user = await _userManager.FindByEmailAsync(email);
                if (user != null)
                {
                    foreach (var stock in Enum.GetValues(typeof(StocksList)))
                    {
                        await _uow.StockDataRepository.CreateAsync(new StockData { UserId = user.Id, Stock = (StocksList)stock });
                    }
                    await _uow.SaveAsync();
                }
                return true;
            }
            catch
            {
                return false;
            }
            
            
        }
    }
}
