﻿using submitter.BLL.Models;
using submitter.BLL.Services.Contracts;
using submitter.DAL;
using submitter.DAL.UOW;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using submitter.BLL.Utils;
using submitter.DAL.Enums;
using System.IO;
using submitter.DAL.Models;

namespace submitter.BLL.Services
{
    public class SubmitService : ISubmitService
    {
        private readonly IMetadataService _metadataService;
        private readonly IUOW _uow;
        private readonly ICryptService _cryptService;
        private readonly IDirectoryService _directoryService;
        private readonly IUploadToStockService _uploadToStockService;
        private readonly IUploadResultService _uploadResultService;
        private string UserId { get; }

        public SubmitService(IMetadataService metadataService, IUOW uow, ICryptService cryptService,
            IDirectoryService directoryService, IUserService userService,
            IUploadToStockService uploadToStockService, IUploadResultService uploadResultService)
        {
            _metadataService = metadataService;
            _uow = uow;
            _cryptService = cryptService;
            _directoryService = directoryService;
            UserId = userService.GetCurrentUserId();
            _uploadToStockService = uploadToStockService;
            _uploadResultService = uploadResultService;
        }

        public async Task<bool> SubmitImagesAsync(string[] items)
        {
            await PrepareImagesToSubmit(items);


            var data = await _uow.StockDataRepository.GetAsync(p => p.UserId == UserId);
            var decryptedData = _cryptService.Decrypt(data);

            var taskList = new List<Task>();

            foreach (var item in Enum.GetValues(typeof(StocksList)))
            {
                var stockData = decryptedData.FirstOrDefault(p => p.Stock == (StocksList)item);
                if (stockData.Permission && stockData.Password != null && stockData.Login != null)
                {
                    taskList.Add(new Task(
                   () =>
                   {
                       _uploadToStockService.Start(stockData, items);
                   }
                   ));
                }
                else
                {
                    foreach (var file in items)
                    {
                        _uploadResultService.SetResult(stockData, file, StatusList.NotSelected);
                    }
                }
            }

            if (taskList.Count > 0)
            {
                foreach (var task in taskList)
                {
                    task.Start();
                }
                Task.WaitAll(taskList.ToArray());
            }

            return true;
        }

        public async Task<ResultModel> SubmitImageAsync(string name, string stock)
        {
            var stockData = await _uow.StockDataRepository.GetFirstOrDefaultAsync(p => p.UserId == UserId && Enum.GetName(typeof(StocksList),p.Stock) == stock);
            if (stockData == null)
            {
                return new ResultModel { Result = false, ErrorMessage = "Can't find stock data" };
            }

            await SetImageStatus(name, stockData.Stock);

            stockData = _cryptService.Decrypt(stockData);
            var task = new Task(
                   () =>
                   {
                       _uploadToStockService.Start(stockData, new string[] { name });
                   }
                   );
            task.Start();
            
            return new ResultModel { Result = true };
        }

        private async Task PrepareImagesToSubmit(string[] items)
        {
            foreach (var item in items)
            {
                var image = await _uow.AppImageRepository.GetFirstOrDefaultAsync(p => p.Name == item && p.UserId == UserId);
                await MarkFileAsSubmitted(image);
                await SetDefaultStatuses(image);
            }
            foreach (var item in items)
            {
                await _metadataService.AppendMetadataToFile(item);
            }
            foreach (var item in items)
            {
                PackUtil.PackFile(item, _directoryService.RootFolder);
            }
            foreach (var item in items)
            {
                OriginalsUtil.RemoveOriginals(_directoryService.RootFolder);
            }
            foreach (var item in items)
            {
                CopyFileForGetty(item);
            }
        }

        private async Task SetImageStatus(string name, StocksList stock)
        {
            var image = await _uow.AppImageRepository.GetFirstOrDefaultAsync(p => p.Name == name && p.UserId == UserId);
            var status = await _uow.ImageStatusRepository.GetFirstOrDefaultAsync(p => p.AppImageId == image.Id && p.Stock == stock);
            image.IsSubmitCompleted = false;
            image.IsSubmitted = true;
            status.Status = StatusList.InProcess;
            await _uow.SaveAsync();
        }
        private async Task MarkFileAsSubmitted(AppImage image)
        {
            image.IsSubmitted = true;
            image.IsSubmitCompleted = false;
            await _uow.SaveAsync();
        }

        private async Task SetDefaultStatuses(AppImage image)
        {
            image.SubmitDateTime = DateTime.Now;
            var statuses = await _uow.ImageStatusRepository.GetAsync(p => p.AppImageId == image.Id);
            var stockDataList = await _uow.StockDataRepository.GetAsync(p => p.UserId == UserId);
            foreach (var status in statuses)
            {
                var stockData = stockDataList.FirstOrDefault(p => p.Stock == status.Stock);
                if (stockData.Permission)
                {
                    status.Status = StatusList.InProcess;
                    status.SubmitDateTime = DateTime.Now;
                }
            }
           await _uow.SaveAsync();
        }

        public void CopyFileForGetty(string file)
        {
            var gettyFolder = @"c:/Getty/";

            try
            {
                if (!Directory.Exists(gettyFolder))
                {
                    Directory.CreateDirectory(@"c:/Getty/");
                }
                if (File.Exists(gettyFolder + file))
                {
                    File.Delete(gettyFolder + file);
                }
                File.Copy(_directoryService.RootFolder + file, gettyFolder + file);
            }
            catch (Exception ex)
            {
                throw new Exception($"GettyError, {ex.Message}");
            }


        }
    }
}
