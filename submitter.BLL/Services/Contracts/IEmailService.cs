﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace submitter.BLL.Services.Contracts
{
    public interface IEmailService
    {
        Task SendEmailAsync(string email, string code);
        Task SendRecoveryPasswordEmailAsync(string email, string code);
    }
}
