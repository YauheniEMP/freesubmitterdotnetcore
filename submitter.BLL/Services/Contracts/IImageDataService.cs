﻿using submitter.BLL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace submitter.BLL.Services.Contracts
{
    public interface IImageDataService
    {
        string GetPreviewPath(string imageName);
        Task<string> GetTitle(string fileName);
        Task SetKeywords(string fileName, string[] keywords);
        Task<string[]> GetKeywords(string fileName);
        Task SetTitle(string fileName, string title);
        Task<bool> UpdateImageDataAsync(UpdateItemModel[] items);
    }
}
