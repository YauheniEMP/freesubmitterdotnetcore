﻿using Microsoft.AspNetCore.Http;
using submitter.BLL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace submitter.BLL.Services.Contracts
{
    public interface IUploadService
    {
        Task<UploadResult> UploadFileAsync(IFormFile file);
    }
}
