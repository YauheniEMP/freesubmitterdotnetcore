﻿using Microsoft.AspNetCore.Identity;
using submitter.BLL.DTO;
using submitter.BLL.Models;
using submitter.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace submitter.BLL.Services.Contracts
{
    public interface IAccountsService
    {
        Task<ResultModel> RegisterUser(string email, string password);
        Task<bool> LoginUser(string email, string password);
        Task LogoutUser();
        Task SendEmailConfirmMessage(string email);
        Task<bool> ConfirmEmailAsync(string email, string code);
        Task<bool> IsEmailConfirmed();
        Task<bool> IsEmailConfirmed(string email);
        Task<ResultModel> SendRecoveryPasswordEmail(string email);
        Task<ResultModel> ResetPassword(string email, string code, string password, string confirmPassword);

        Task AuthenticateUser(string email);
    }
}
