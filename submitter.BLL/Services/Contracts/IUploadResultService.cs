﻿using submitter.DAL.Enums;
using submitter.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace submitter.BLL.Services.Contracts
{
    public interface IUploadResultService
    {
        void SetResult(StockData data, string fileName, StatusList result);
    }
}
