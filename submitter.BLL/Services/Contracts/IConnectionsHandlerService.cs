﻿using submitter.BLL.Models.ConnectionModels.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace submitter.BLL.Services.Contracts
{
    public interface IConnectionsHandlerService
    {
        List<IConnection> Connections { get; set; }
        IConnection Add(IConnection connection);
        void Remove(IConnection connection);
    }
}
