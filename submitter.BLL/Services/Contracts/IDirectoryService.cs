﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace submitter.BLL.Services.Contracts
{
    public interface IDirectoryService
    {
        string RootFolder { get; }
        string PreviewFolder { get; }
        string WatermarkPath { get; }
        string ExifToolPath { get; }
        string GhostScriptPath { get; }
    }
}
