﻿using submitter.BLL.Models;
using System.Collections.Generic;
using System.Threading.Tasks;
using submitter.BLL.DTO;
namespace submitter.BLL.Services.Contracts
{
    public interface IFilesService
    {
        Task<LastSubmittedFilesModel> GetLastSubmittedFiles(int page);
        Task<bool> DeleteFilesAsync(string[] fileNames);
        Task<List<NotSubmittedImageModel>> GetNotSubmittedImages();
        Task<List<HistoryImageDTO>> GetProcessingFiles();
    }
}
