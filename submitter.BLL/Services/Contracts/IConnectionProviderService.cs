﻿using submitter.BLL.Models.ConnectionModels.Contracts;
using submitter.DAL.Enums;
using submitter.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace submitter.BLL.Services.Contracts
{
    public interface IConnectionProviderService
    {
        IConnection GetConnection(StockData data,UploadType uploadType);
        void RemoveConnection(IConnection connection);
    }
}
