﻿using submitter.BLL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace submitter.BLL.Services.Contracts
{
    public interface IStockDataService
    {
        Task<List<StockDataModel>> GetStockData();
        Task<bool> SaveStockData(List<StockDataModel> list);
        Task<bool> SaveStockItem(StockDataModel item);
        Task<bool> CreateStockData(string email);
    }
}
