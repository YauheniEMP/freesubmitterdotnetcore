﻿using submitter.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace submitter.BLL.Services.Contracts
{
    public interface IUploadToStockService
    {
        void Start(StockData data, string[] fileNames);
    }
}
