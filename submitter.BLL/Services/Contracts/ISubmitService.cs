﻿using submitter.BLL.Models;
using submitter.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace submitter.BLL.Services.Contracts
{
    public interface ISubmitService
    {
        Task<bool> SubmitImagesAsync(string[] items);
        Task<ResultModel> SubmitImageAsync(string name, string stock);
    }
}
