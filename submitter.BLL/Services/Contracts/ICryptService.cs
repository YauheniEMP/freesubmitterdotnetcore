﻿using submitter.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace submitter.BLL.Services.Contracts
{
    public interface ICryptService
    {
        string Encrypt(string str);
        string Decrypt(string str);
        List<StockData> Decrypt(List<StockData> list);
        List<StockData> Encrypt(List<StockData> list);
        StockData Encrypt(StockData data);
        StockData Decrypt(StockData data);
    }
}
