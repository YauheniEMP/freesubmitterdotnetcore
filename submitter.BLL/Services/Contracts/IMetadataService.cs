﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace submitter.BLL.Services.Contracts
{
    public interface IMetadataService
    {
        Task SetKeywords(string fileName, string[] keywords);
        Task<string[]> GetKeywords(string fileName);
        Task<string> GetTitle(string fileName);
        Task SetTitle(string fileName, string title);
        Task AppendMetadataToFile(string fileName);

    }
}
