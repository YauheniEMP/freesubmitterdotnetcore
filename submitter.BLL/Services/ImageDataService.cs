﻿using submitter.BLL.Services.Contracts;
using submitter.DAL.UOW;
using System.Threading.Tasks;
using submitter.BLL.Models;
using submitter.BLL.Extensions;

namespace submitter.BLL.Services
{
    public class ImageDataService:IImageDataService
    {
        private readonly IUOW _uow;
        private readonly IDirectoryService _directoryService;

        public ImageDataService(IUOW uow, IDirectoryService directoryService, IUserService userService)
        {
            _uow = uow;
            _directoryService = directoryService;
            UserId = userService.GetCurrentUserId();
        }

        private string UserId { get; }
        public async Task<string> GetTitle(string fileName)
        {
            var image = await _uow.AppImageRepository.GetFirstOrDefaultAsync(p=>p.Name == fileName.SetEpsExtension() && p.UserId == UserId);
            return image.Title;
        }

        public async Task<string[]> GetKeywords(string fileName)
        {
            var image = await _uow.AppImageRepository.GetFirstOrDefaultAsync(p=>p.Name == fileName.SetEpsExtension() && p.UserId == UserId);
            return image.Keywords.GetArrayFromString();

        }
        public async Task SetKeywords(string fileName, string[] keywords)
        {
            var image = await _uow.AppImageRepository.GetFirstOrDefaultAsync(p=>p.Name == fileName.SetEpsExtension() && p.UserId == UserId);
            if (image.UserId != UserId)
            {
                throw new System.Exception("Access denied");
            }
            image.Keywords = keywords.GetStringFromArray();
            await _uow.SaveAsync();
        }

        public async Task SetTitle(string fileName, string title)
        {
            var image = await _uow.AppImageRepository.GetFirstOrDefaultAsync(p => p.Name == fileName.SetEpsExtension() && p.UserId == UserId);
            if (image.UserId != UserId)
            {
                throw new System.Exception("Access denied");
            }
            image.Title = title;
            await _uow.SaveAsync();
        }
        public string GetPreviewPath(string imageName)
        {
            return _directoryService.PreviewFolder + imageName.SetJpgExtension();
        }

        public async Task<bool> UpdateImageDataAsync(UpdateItemModel[] items)
        {
            foreach (var item in items)
            {
                await SetTitle(item.Name, item.Title);
                await SetKeywords(item.Name, item.Keywords);
            }
            return true;
        }
    }
}
