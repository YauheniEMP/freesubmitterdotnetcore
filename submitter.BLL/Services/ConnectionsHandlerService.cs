﻿using submitter.BLL.Models.ConnectionModels.Contracts;
using submitter.BLL.Services.Contracts;
using System.Collections.Generic;

namespace submitter.BLL.Services
{
    public class ConnectionsHandlerService : IConnectionsHandlerService
    {
        public List<IConnection> Connections { get; set; } = new List<IConnection>();

        public IConnection Add(IConnection connection)
        {
            Connections.Add(connection);
            return connection;
        }

        public void Remove(IConnection connection)
        {
            Connections.Remove(connection);
        }
    }
}
