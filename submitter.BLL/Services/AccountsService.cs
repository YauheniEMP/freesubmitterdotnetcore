﻿using Microsoft.AspNetCore.Identity;
using submitter.BLL.Models;
using submitter.BLL.Services.Contracts;
using submitter.DAL.Models;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace submitter.BLL.Services
{
    public class AccountsService : IAccountsService
    {
        private readonly UserManager<User> _userManager;
        private readonly SignInManager<User> _signInManager;
        private readonly IEmailService _emailService;
        private readonly IUserService _userService;
        private readonly IStockDataService _stockDataService;

        public AccountsService(UserManager<User> userManager, SignInManager<User> signInManager, IEmailService emailService,
             IUserService userService, IStockDataService stockDataService)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _emailService = emailService;
            _userService = userService;
            _stockDataService = stockDataService;
        }

        public async Task<ResultModel> RegisterUser(string email, string password)
        {
            var result = await _userManager.CreateAsync(new User { UserName = email, Email = email }, password);
            if (result.Succeeded)
            {
                var dataResult = await _stockDataService.CreateStockData(email);
                if (dataResult)
                {
                    try
                    {
                        await AuthenticateUser(email);
                        await SendEmailConfirmMessage(email);
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    };

                    return new ResultModel { Result = true };
                }
                else
                {
                    var user = await _userManager.FindByEmailAsync(email);
                    if (user != null)
                    {
                        await _userManager.DeleteAsync(user);
                    }
                    return new ResultModel { Result = false, ErrorMessage = "Unexpected error" };
                }
            }
            else
            {
                return new ResultModel { Result = false, ErrorMessage = result.Errors.FirstOrDefault()?.Description ?? "Unexpected error" };
            }
        }

        public async Task<bool> ConfirmEmailAsync(string email, string code)
        {
            var user = await _userManager.FindByEmailAsync(email);
            var result = await _userManager.ConfirmEmailAsync(user, code.Replace(" ", "+"));
            if (result.Succeeded)
            {
                await _signInManager.SignInAsync(user, true);
                return true;
            }
            else
            {
                return false;
            }
        }

        public async Task<bool> IsEmailConfirmed()
        {
            var user = await _userService.GetCurrentUser();
            return await _userManager.IsEmailConfirmedAsync(user);
        }

        public async Task<bool> IsEmailConfirmed(string email)
        {
            var user = await _userManager.FindByEmailAsync(email);
            return await _userManager.IsEmailConfirmedAsync(user);
        }

        public async Task<bool> LoginUser(string email, string password)
        {
            var result = await _signInManager.PasswordSignInAsync(email, password, true, true);
            if (result.Succeeded)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public async Task AuthenticateUser(string email)
        {
            var user = await _userManager.FindByEmailAsync(email);
            await _signInManager.SignInAsync(user, true);
        }
        public async Task LogoutUser()
        {
            await _signInManager.SignOutAsync();
        }

        public async Task SendEmailConfirmMessage(string email)
        {
            var user = await _userManager.FindByEmailAsync(email);
            var code = await _userManager.GenerateEmailConfirmationTokenAsync(user);
            await _emailService.SendEmailAsync(email, code);
        }

        public async Task<ResultModel> SendRecoveryPasswordEmail(string email)
        {
            if (email == null)
            {
                return new ResultModel { ErrorMessage = "Wrong email", Result = false };
            }
            var user = await _userManager.FindByEmailAsync(email);
            if (user == null)
            {
                return new ResultModel { ErrorMessage = "Wrong email", Result = false };
            }
            var code = await _userManager.GeneratePasswordResetTokenAsync(user);
            await _emailService.SendRecoveryPasswordEmailAsync(email, code);
            return new ResultModel { Result = true };
        }

        public async Task<ResultModel> ResetPassword(string email, string code, string password, string confirmPassword)
        {
            if (email == null || code == null)
            {
                return new ResultModel { ErrorMessage = "Server error", Result = false };
            }
            if (password != confirmPassword)
            {
                return new ResultModel { ErrorMessage = "Password mismatch", Result = false };
            }
            var user = await _userManager.FindByEmailAsync(email);
            if (user == null)
            {
                return new ResultModel { ErrorMessage = "Server error", Result = false };
            }

            var result = await _userManager.ResetPasswordAsync(user, code.Replace(" ", "+"), password);
            if (result.Succeeded)
            {
                return new ResultModel { Result = true };
            }
            else
            {
                return new ResultModel { ErrorMessage = "Server error", Result = false };
            }

        }
    }
}
