﻿using Microsoft.AspNetCore.Hosting;
using submitter.BLL.Services.Contracts;

namespace submitter.BLL.Services
{
    public class DirectoryService : IDirectoryService
    {
        public string RootFolder { get; }
        public string PreviewFolder { get; }
        public string WatermarkPath { get; }
        public string ExifToolPath { get; }
        public string GhostScriptPath { get; }

        private readonly IHostingEnvironment _hostingEnvironment;
        private string UserId { get; }

        public DirectoryService(IHostingEnvironment hostingEnvironment,IUserService userService)
        {
            _hostingEnvironment = hostingEnvironment;
            UserId = userService.GetCurrentUserId();
            RootFolder = GetRootFolder();
            PreviewFolder = GetPreviewFolder();
            WatermarkPath = GetWatermarkPath();
            ExifToolPath = GetExifToolPath();
            GhostScriptPath = GetGhostScriptPath();
        }

        private string GetRootFolder()
        {
            return _hostingEnvironment.ContentRootPath + "\\" + "Resources" + "\\" + "Images" + "\\" + UserId + "\\";
        }

        private string GetWatermarkPath()
        {
            return _hostingEnvironment.ContentRootPath + "\\" + "Resources" + "\\" + "Images" + "\\" + "watermark.png";
        }

        private string GetPreviewFolder()
        {
            return _hostingEnvironment.ContentRootPath + "\\" + "Resources" + "\\" + "Images" + "\\" + UserId + "\\" + "PRW" + "\\";
        }

        private string GetExifToolPath()
        {
            return _hostingEnvironment.ContentRootPath + "\\" + "Resources" + "\\" + "exiftool.exe";
        }

        private string GetGhostScriptPath()
        {
            return _hostingEnvironment.ContentRootPath + "\\" + "Resources" + "\\" + "gs9.27" + "\\" + "bin" + "\\";
        }
    }
}
