﻿using submitter.DAL.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace submitter.BLL.Services.Contracts
{
    public class LocalCryptService:ICryptService
    {
        private readonly string _code = "qlqlRasd4aserePAFFS22311";

        public string Encrypt(string str)
        {
            if (str == null)
            {
                return null;
            }
            byte[] clearBytes = Encoding.Unicode.GetBytes(str);
            using (Aes encryptor = Aes.Create())
            {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(_code, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(clearBytes, 0, clearBytes.Length);
                        cs.Close();
                    }
                    str = Convert.ToBase64String(ms.ToArray());
                }
            }
            return str;
        }

        public string Decrypt(string str)
        {
            if (str == null)
            {
                return null;
            }
            str = str.Replace(" ", "+");
            byte[] cipherBytes = Convert.FromBase64String(str);
            using (Aes encryptor = Aes.Create())
            {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(_code, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateDecryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(cipherBytes, 0, cipherBytes.Length);
                        cs.Close();
                    }
                    str = Encoding.Unicode.GetString(ms.ToArray());
                }
            }
            return str;
        }

        public List<StockData> Encrypt(List<StockData> list)
        {
            if (list.Count == 0)
            {
                return null;
            }
            var newList = new List<StockData>();
            foreach (var item in list)
            {
                newList.Add(new StockData
                {
                    User = item.User,
                    UserId = item.UserId,
                    Id = item.Id,
                    Login = Encrypt(item.Login),
                    Password = Encrypt(item.Password),
                    Permission = item.Permission,
                    Stock = item.Stock
                });
            }
            return newList;
        }

        public List<StockData> Decrypt(List<StockData> list)
        {
            if (list.Count == 0)
            {
                return null;
            }
            var newList = new List<StockData>();
            foreach (var item in list)
            {
                newList.Add(new StockData
                {
                    User = item.User,
                    UserId = item.UserId,
                    Id = item.Id,
                    Login = Decrypt(item.Login),
                    Password = Decrypt(item.Password),
                    Permission = item.Permission,
                    Stock = item.Stock
                });
            }
            return newList;
        }

        public StockData Encrypt(StockData data)
        {
            if (data == null)
            {
                return null;
            }
            return new StockData
            {
                User = data.User,
                UserId = data.UserId,
                Id = data.Id,
                Login = Encrypt(data.Login),
                Password = Encrypt(data.Password),
                Permission = data.Permission,
                Stock = data.Stock
            };
        }

        public StockData Decrypt(StockData data)
        {
            if (data == null)
            {
                return null;
            }
            return new StockData
            {
                User = data.User,
                UserId = data.UserId,
                Id = data.Id,
                Login = Decrypt(data.Login),
                Password = Decrypt(data.Password),
                Permission = data.Permission,
                Stock = data.Stock
            };
        }
    }
}
