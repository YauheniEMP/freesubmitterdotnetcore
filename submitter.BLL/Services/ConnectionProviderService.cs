﻿using submitter.BLL.Models.ConnectionModels.Contracts;
using submitter.BLL.Services.Contracts;
using submitter.BLL.Utils;
using submitter.DAL.Enums;
using submitter.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace submitter.BLL.Services
{
    public class ConnectionProviderService : IConnectionProviderService
    {
        private readonly IConnectionsHandlerService _connectionsHandlerService;
        private readonly IUploadResultService _resultService;
        private readonly IDirectoryService _directoryService;

        public ConnectionProviderService(IConnectionsHandlerService connectionsHandlerService, IUploadResultService resultService,
            IDirectoryService directoryService)
        {
            _connectionsHandlerService = connectionsHandlerService;
            _resultService = resultService;
            _directoryService = directoryService;
        }

        public IConnection GetConnection(StockData data, UploadType uploadType)
        {
            IConnection connection;
            int connectionCount;
            lock (_connectionsHandlerService.Connections)
            {
                connection = _connectionsHandlerService.Connections?.FirstOrDefault(p => p.UploadType == uploadType && p.Data.UserId == data.UserId && p.Data.Stock == data.Stock);
                connectionCount = _connectionsHandlerService.Connections?.Count ?? 0;
                if (connectionCount == 0 || connection == null)
                {
                    var newConnection = ConnectionFactoryUtil.CreateConnection(data, uploadType)
                        .AddEmmitter(_resultService)
                        .AddRootPath(_directoryService.RootFolder);
                    _connectionsHandlerService.Add(newConnection);
                    return newConnection;
                }
            }
            return connection;
        }

        public void RemoveConnection(IConnection connection)
        {
            lock (_connectionsHandlerService.Connections)
            {
                if (connection != null)
                {
                    _connectionsHandlerService.Remove(connection);
                    connection.Dispose();
                }
            }
        }
    }
}
