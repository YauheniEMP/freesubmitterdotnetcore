﻿using Medallion.Shell;
using submitter.BLL.Services.Contracts;
using System.Threading.Tasks;
using submitter.DAL.UOW;
using submitter.BLL.Extensions;

namespace submitter.BLL.Services
{
    public class MetadataService : IMetadataService
    {
        private readonly IDirectoryService _directoryService;
        private readonly IUOW _uow;
        string UserId { get; }

        public MetadataService(IDirectoryService directoryService, IUOW uow, IUserService userService)
        {
            _uow = uow;
            _directoryService = directoryService;
            UserId = userService.GetCurrentUserId();
        }

        public async Task SetKeywords(string fileName, string[] keywords)
        {
            var path = _directoryService.RootFolder + fileName;
            var cmd = Command.Run(_directoryService.ExifToolPath, "-IPTC:Keywords=", path);
            await cmd.Task;

            foreach (var keyword in keywords)
            {
                cmd = Command.Run(_directoryService.ExifToolPath, $"-IPTC:Keywords+={keyword}", path);
                await cmd.Task;
            }
        }

        public async Task SetTitle(string fileName, string title)
        {
            var path = _directoryService.RootFolder + fileName;

            var cmd = Command.Run(_directoryService.ExifToolPath, $"-IPTC:ObjectName={title} ", path);
            await cmd.Task;

            //var cmd = Command.Run(_directoryService.ExifToolPath, $"-IPTC:Headline={title}", path);
            //await cmd.Task;
            cmd = Command.Run(_directoryService.ExifToolPath, $"-IPTC:Caption-Abstract={title}", path);
            await cmd.Task;

            //if (fileName.EndsWith('s'))
            //{
            //    cmd = Command.Run(_directoryService.ExifToolPath, $"-PostScript:Title={title}", path);
            //    await cmd.Task;
            //}

            if (fileName.EndsWith('g'))
            {
                cmd = Command.Run(_directoryService.ExifToolPath, $"-XMP-dc:Title={title}  ", path);
                await cmd.Task;
                cmd = Command.Run(_directoryService.ExifToolPath, $"-XMP-dc:Description={title} Vector illustration.", path);
                await cmd.Task;
            }

        }


        public async Task<string> GetTitle(string fileName)
        {
            var path = _directoryService.RootFolder + fileName;
            var cmd = Command.Run(_directoryService.ExifToolPath, "-IPTC:ObjectName", path);
            var result = await cmd.Task;
                     
            return GetFormattedTitle(result.StandardOutput);
        }

        public async Task<string[]> GetKeywords(string fileName)
        {
            var path = _directoryService.RootFolder + fileName;
            var cmd = Command.Run(_directoryService.ExifToolPath, "-IPTC:Keywords", path);
            var result = await cmd.Task;

            var output = result.StandardOutput;
            if (output == "")
            {
                return null;
            }
            return GetArrayKeywords(output);
        }

        public async Task AppendMetadataToFile(string fileName)
        {
            var fileData = await _uow.AppImageRepository.GetFirstOrDefaultAsync(p=>p.Name == fileName && p.UserId == UserId);
            if (fileData.Keywords != null && fileData.Keywords != string.Empty)
            {
                await SetKeywords(fileName, fileData.Keywords.GetArrayFromString());
                await SetKeywords(fileName.Substring(0, fileName.Length - 4) + ".jpg", fileData.Keywords.GetArrayFromString());
            }

            if(fileData.Title != null && fileData.Title != string.Empty)
            {
                await SetTitle(fileName, fileData.Title);
                await SetTitle(fileName.Substring(0, fileName.Length - 4) + ".jpg", fileData.Title);
            }
        }

        private string[] GetArrayKeywords(string standardOutput)
        {
            var keywordsString = standardOutput.Replace(" ", "");
            return keywordsString
            .Substring(keywordsString.IndexOf(":") + 1, keywordsString.Length - keywordsString.IndexOf(":") - 3)
            .Split(",");
        }

        private string GetFormattedTitle(string standardOutput)
        {
            if (standardOutput == null  || standardOutput == string.Empty)
            {
                return standardOutput;
            }
           
            return standardOutput
            .Substring(standardOutput.IndexOf(":") + 1, standardOutput.Length - standardOutput.IndexOf(":") - 3);
        }
    }
}
