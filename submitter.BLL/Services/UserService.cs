﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using submitter.BLL.Services.Contracts;
using submitter.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace submitter.BLL.Services
{
    public class UserService : IUserService
    {
        private readonly UserManager<User> _userManager;
        private readonly ClaimsPrincipal principal;

        public UserService(IHttpContextAccessor httpContextAccessor, UserManager<User> userManager)
        {
            _userManager = userManager;
            principal = httpContextAccessor.HttpContext.User as ClaimsPrincipal;
        }

        public async Task<User> GetCurrentUser()
        {
            return await _userManager.FindByEmailAsync(_userManager.GetUserName(principal));
        }

        public string GetCurrentUserId()
        {
            return _userManager.GetUserId(principal);
        }

        public string GetCurrentUserName()
        {
            return _userManager.GetUserName(principal);
        }
    }
}
