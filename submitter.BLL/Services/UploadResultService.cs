﻿using submitter.BLL.Services.Contracts;
using submitter.DAL.Enums;
using submitter.DAL.Models;
using submitter.DAL.UOW;
using System.Linq;

namespace submitter.BLL.Services
{
    public class UploadResultService: IUploadResultService
    {
        private readonly IUOW _uow;
        public UploadResultService(IUOW uow)
        {
            _uow = uow;
        }

        public void SetResult(StockData data, string fileName, StatusList result)
        {
            lock (_uow)
            {
                var image = _uow.AppImageRepository.GetFirstOrDefault(p => p.Name == fileName && p.UserId == data.UserId);
                var imageData = _uow.ImageStatusRepository.GetFirstOrDefault(p => p.AppImageId == image.Id && p.Stock == data.Stock);

                imageData.Status = result;
                _uow.Save();
                var allImageData = _uow.ImageStatusRepository.Get(p => p.AppImageId == image.Id);
                if (allImageData.FirstOrDefault(p => p.Status == StatusList.InProcess) == null)
                {
                    image.IsSubmitCompleted = true;
                    _uow.Save();
                }
            }
        }

    }
}
