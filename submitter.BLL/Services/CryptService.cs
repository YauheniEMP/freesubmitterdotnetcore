﻿using Microsoft.AspNetCore.DataProtection;
using submitter.BLL.Services.Contracts;
using submitter.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace submitter.BLL.Services
{
    public class CryptService : ICryptService
    {
        private readonly IDataProtectionProvider _provider;
        private readonly string _code = "ASDFJASFNSFSDB53535SDFSDFSDFSQQRPKMDMFMskdfk554";
        public CryptService(IDataProtectionProvider provider)
        {
            _provider = provider;
        }

        public string Encrypt(string str)
        {
            if (str == null)
            {
                return null;
            }
            var protector = _provider.CreateProtector(_code);
            return protector.Protect(str);
        }

        public string Decrypt(string str)
        {
            if (str == null)
            {
                return null;
            }
            var protector = _provider.CreateProtector(_code);
            return protector.Unprotect(str);
        }

        public List<StockData> Encrypt(List<StockData> list)
        {
            if (list.Count == 0)
            {
                return null;
            }
            var newList = new List<StockData>();
            foreach (var item in list)
            {
                newList.Add(new StockData
                {
                    User = item.User,
                    UserId = item.UserId,
                    Id = item.Id,
                    Login = Encrypt(item.Login),
                    Password = Encrypt(item.Password),
                    Permission = item.Permission,
                    Stock = item.Stock
                });
            }
            return newList;
        }

        public List<StockData> Decrypt(List<StockData> list)
        {
            if (list.Count == 0)
            {
                return null;
            }
            var newList = new List<StockData>();
            foreach (var item in list)
            {
                newList.Add(new StockData
                {
                    User = item.User,
                    UserId = item.UserId,
                    Id = item.Id,
                    Login = Decrypt(item.Login),
                    Password = Decrypt(item.Password),
                    Permission = item.Permission,
                    Stock = item.Stock
                });
            }
            return newList;
        }

        public StockData Encrypt(StockData data)
        {
            if (data == null)
            {
                return null;
            }
            return new StockData
            {
                User = data.User,
                UserId = data.UserId,
                Id = data.Id,
                Login = Encrypt(data.Login),
                Password = Encrypt(data.Password),
                Permission = data.Permission,
                Stock = data.Stock
            };
        }

        public StockData Decrypt(StockData data)
        {
            if (data == null)
            {
                return null;
            }
            return new StockData
            {
                User = data.User,
                UserId = data.UserId,
                Id = data.Id,
                Login = Decrypt(data.Login),
                Password = Decrypt(data.Password),
                Permission = data.Permission,
                Stock = data.Stock
            };
        }
    }
}
